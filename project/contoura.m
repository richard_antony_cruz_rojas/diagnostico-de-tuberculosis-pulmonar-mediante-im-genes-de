function z = contoura( path )
    I = imread(path);
    I = rgb2gray(I);
    figure;
    imcontour(I,3)
end