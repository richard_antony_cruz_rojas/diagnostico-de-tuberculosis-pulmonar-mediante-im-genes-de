function z = soloPulmones(path)
    img = imread(path);
    img = im2bw(img);
    mask = [1 0 0 0 0 0 0 1;1 0 0 0 0 0 0 1;1 0 0 0 0 0 0 1;1 0 0 0 0 0 0 1;1 0 0 0 0 0 0 1;1 0 0 0 0 0 0 1;1 0 0 0 0 0 0 1;1 1 1 1 1 1 1 1];
    maskTotal = [1 1; 1 1];
    mask1_4 = [0 1; 1 1];
    mask2_4 = [1 0; 1 1];
    mask3_4 = [1 1; 0 1];
    mask4_4 = [1 1; 1 0];
    
    maskTotal = resizem(maskTotal,size(img));
    mask = resizem(mask,size(img));
    mask1_4 = resizem(mask1_4,size(img));
    mask2_4 = resizem(mask2_4,size(img));
    mask3_4 = resizem(mask3_4,size(img));
    mask4_4 = resizem(mask4_4,size(img));
    img = img + mask;
    
    subplot(4,2,1:4);
    imshow(img);
    subplot(4,2,5);
    part1 = mask1_4 + img;
    imshow(part1);
    subplot(4,2,6);
    part2 = mask2_4 + img;
    imshow(part2);
    subplot(4,2,7);
    part3 = mask3_4 + img;
    imshow(part3);
    subplot(4,2,8);
    part4 = mask4_4 + img;
    imshow(part4);
    
    z = [bwarea(~part1) bwarea(~part2); bwarea(~part3) bwarea(~part4)];
    z = z / bwarea(maskTotal);
    z = z * 100;
end