function z = imageSegmentation( path )
    image = imread(path);
    I = rgb2gray(image);
    [~, threshold] = edge(imcomplement(I), 'roberts');
    fudgeFactor = .5;
    BWs = edge(imcomplement(I),'roberts', threshold * fudgeFactor);
    se90 = strel('line', 3, 90);
    se0 = strel('line', 3, 0);
    BWsdil = imdilate(BWs, [se90 se0]);
    BWdfill = imfill(BWsdil, 'holes');
    BWnobord = imclearborder(BWdfill, 4);
    seD = strel('diamond',1);
    BWfinal = imerode(BWnobord,seD);
    BWfinal = imerode(BWfinal,seD);
    BWoutline = bwperim(BWfinal);
    Segout = I;
    Segout(BWoutline) = 255;
    imshow(BWsdil);
end