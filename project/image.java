import java.awt.*;
import java.awt.image.BufferedImage;

import java.io.*;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class image {


	public static double[] getDataFromImage(String path) {
		BufferedImage  image;
		int width;
		int height;
		double[] data = {0,0,0,0,0};
		double area1 = 0;
		double area2 = 0;
		double area3 = 0;
		double area4 = 0;
		double relation = 0;
		
		try {
			File input = new File(path);
			image = ImageIO.read(input);
			width = image.getWidth();
			height = image.getHeight();
			int borderW = width/15;
			int borderH = width/25;

			for(int i=0; i<height; i++){

				for(int j=0; j<width; j++){

					Color c = new Color(image.getRGB(j, i));
					int red = (int)(c.getRed() * 0.299);
					int green = (int)(c.getGreen() * 0.587);
					int blue = (int)(c.getBlue() *0.114);
					int gray = red+green+blue;
					gray = (int)(fuzzyGray(gray,90,140,255)*255);
					
					if(j < borderW || j > (width - borderW) || i < borderH || i > (height - borderH))
						gray = 255;
					
					Color newColor = new Color(gray,gray,gray);

					image.setRGB(j,i,newColor.getRGB());
				}
			}

			BufferedImage p1 = image.getSubimage(0, 0, width/2, height/2);
			BufferedImage p2 = image.getSubimage(width/2, 0, width/2, height/2);
			BufferedImage p3 = image.getSubimage(0, height/2, width/2, height/2);
			BufferedImage p4 = image.getSubimage(width/2, height/2, width/2, height/2);

			area1 = getAreaImage(p1);
			
			area2 = getAreaImage(p2);
			
			area3 = getAreaImage(p3);
			
			area4 = getAreaImage(p4);
			
			data[0] = area1;
			data[1] = area2;
			data[2] = area3;
			data[3] = area4;
			data[4] = (area1 + area2)/(area3 + area4);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
	private static double getAreaImage(BufferedImage image){
		double width = (double) image.getWidth();
		double height = (double) image.getHeight();
		double count = 0.0;
		
		for(int i=0; i<height; i++){
			for(int j=0; j<width; j++){
				Color c = new Color(image.getRGB(j, i));
				int color = c.getRed();
				if(color > 0)
					count++;
			}
		}
		return count/(width * height);
	}
	
	private static double fuzzyGray(int gray,int bottom,int middle,int top){
		if(gray < bottom)
			return 0;
		if(gray >= bottom && gray <=middle)
			return 2*Math.pow((gray-bottom)/(top-bottom),2);
		if(gray > middle && gray <= top)
			return 1-2*Math.pow((gray-top)/(top-bottom),2);
		return 1;
	}

	static public void main(String args[]) throws Exception 
	{
		double[] data = {0,0,0,0,0};
		data = getDataFromImage("3.png");
		System.out.println("Area1: " + data[0]);
		System.out.println("Area2: " + data[1]);
		System.out.println("Area3: " + data[2]);
		System.out.println("Area4: " + data[3]);
		System.out.println("Relation: " + data[4]);
	}
}