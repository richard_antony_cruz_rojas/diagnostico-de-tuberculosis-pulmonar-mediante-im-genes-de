function perc = perceptron(w, patron, alfa)
 x0 = -1;
 %x0 = 0;
 i = 1;
 patron=[x0*ones(size(patron,1),1):patron];
 errorg=[1 1 1 1];
 while sum(errorg)>0
  for i=1:size(patron,1)
   salida = evalperc(w,patron(i,[1:(size(patron,2)-1)]));
   error = patron(i,size(patron,2))-salida;
   error(i) = 0;
   if error~=0
    error(i)=1;
    while error~=0
     salida=evalperc(w,patron(i,[1:(size(patron,2)-1)]));
     error=patron(i,size(patron,2))-salida;
     w=w+alfa*error*patron(i,[1:(size(patron,2)-1)]);
    end
  end
 %errorg
 end
 perc=w;
end
