function salida=evalperc(w,inputs)
 nets=[inputs]*w';
 if nets>=0
  salida=1;
 elseif nets<0
  salida=0;
 end
end
