function z = filterAndy( path )
 image = imread(path);
 image = rgb2gray(image);
 andy = edge(image, 'sobel');
 imshow(andy);
end
