/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

/* calculate de/do of each node */
/* for mean square error, 
   de_dout = -2*(target - node_p[Node_n-1]->value) */
void
calculate_de_do(de_dout)
double de_dout;
{
	int i, j;
	NODE_LIST_T *p; 
	double tmp1, tmp2;
	double derivative_o_o();
	double de_do;

	node_p[Node_n - 1]->de_do = de_dout;
	for (i = Node_n - 2; i >= In_n; i--) {
		de_do = 0;
		for (p = node_p[i]->fan_out; p != NULL; p = p->next) {
			j = p->content->index;
			tmp1 = p->content->de_do;
			tmp2 = derivative_o_o(i, j);
			de_do += tmp1*tmp2;
		}
		node_p[i]->de_do = de_do;
	}
}

/* calculate do(i)/do(j), where i and j are node indice */
double
derivative_o_o(i, j)
int i, j;
{
	double do2_do1(), do3_do2(), do4_do3(), do5_do4(); 
	int layer = node_p[i]->layer;
	switch (layer) {
		case 0:
			exit1("Error in derivative_o_o!");
		case 1:
			return(do2_do1(i, j));
		case 2:
			return(do3_do2(i, j));
		case 3: 
			return(do4_do3(i, j));
		case 4: 
			return(do5_do4(i, j));
		default:
			exit1("Error in derivative_o_o!");
	}
}

/* calculate do(j)/do(i), where node i is in layer 4, node j layer 5 */
double
do5_do4(i, j)
int i, j;
{
	return(1.0);
}

/* calculate do(j)/do(i), where node i is in layer 3, node j layer 4 */
double 
do4_do3(i, j)
int i, j;
{
	NODE_LIST_T *arg_p = node_p[j]->fan_in;
	PARAMETER_LIST_T *para_p = node_p[j]->parameter;
	NODE_LIST_T *a_p;
	PARAMETER_LIST_T *p_p;
	int k;
	double x, a, total = 0;

	if ((j - i) != Rule_n)
		exit1("Error in do4_do3()!");
	a_p = arg_p;
	p_p = para_p;
	for (k = 0; k < In_n + 1; k++) {
		x = a_p->content->value;
		a = p_p->content;
		if (k == In_n)
			break;
		total += x*a;
		a_p = a_p->next;
		p_p = p_p->next;
	}
	return(total + a);
	/*
	double a, b, c, x1, x2;
	x1 = arg_p->content->value;
	x2 = arg_p->next->content->value;
	a = para_p->content;
	b = para_p->next->content;
	c = para_p->next->next->content;
	return(a*x1 + b*x2 + c);
	*/
}

/* calculate do(j)/do(i), where node i is in layer 2, node j layer 3 */
double 
do3_do2(i, j)
int i, j;
{
	NODE_LIST_T *arg_p = node_p[j]->fan_in;
	NODE_LIST_T *p;
	double total = 0;

	for (p = arg_p; p != NULL; p = p->next)
		total += p->content->value;

	if ((j - i) == Rule_n)
		return((total - node_p[i]->value)/(total*total));
	else
		return(-node_p[j - Rule_n]->value/(total*total));
}
	
/* calculate do(j)/do(i), where node i is in layer 1, node j layer 2 */
double
do2_do1(i, j)
int i, j;
{
	NODE_LIST_T *arg_p = node_p[j]->fan_in;
	NODE_LIST_T *p;
	double product = 1.0;

	/*
	for (p = arg_p; p != NULL; p = p->next) {
		if (p->content->index == i)
			continue;
		product *= p->content->value;
	}
	return(product);
	*/
	return((node_p[j]->value)/(node_p[i]->value));
}
