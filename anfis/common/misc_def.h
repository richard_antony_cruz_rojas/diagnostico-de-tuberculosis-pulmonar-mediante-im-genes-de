/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

/* file definition */
#define TRAIN_ERR_FILE   "error.trn"
#define CHECK_ERR_FILE   "error.chk"
#define INIT_PARA_FILE   "para.ini"
#define FINA_PARA_FILE   "para.fin"
#define TRAIN_DATA_FILE  "data.trn"
#define CHECK_DATA_FILE  "data.chk"
#define STEP_SIZE_FILE   "stepsize"

/* error type definition, see trn_err.c */
/* 0 --> RMSE (root mean square error)
   1 --> NDEI (non-dimensional error index)
   2 --> ARV  (average relative variance)
   3 --> APE  (average percentage error) */
#define ERROR_TYPE 0
