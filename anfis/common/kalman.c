/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"
static int k_p_n;     /* number of parameters to be identified 
			 by kalman filter algorithm */

/* get the data to be used in kalman filter algorithm */
void
get_kalman_data(kalman_data, target)
double *kalman_data;
double target;
{
	/* 'first' is the index of the first node in layer 3 */
	int i, k, j = 0;
	int first = In_n + In_n*Mf_n + Rule_n; 

	k_p_n = (In_n +1)*Rule_n; 
	for (i = first; i < first + Rule_n; i++) {
		for (k = 0; k < In_n; k++) 
			kalman_data[j++] = (node_p[i]->value)*
					   (node_p[k]->value);
		kalman_data[j++] = node_p[i]->value;
	}
	/*
	double x = node_p[0]->value;
	double y = node_p[1]->value;

	for (i = first; i < first + Rule_n; i++) {
		kalman_data[j++] = node_p[i]->value*x;
		kalman_data[j++] = node_p[i]->value*y;
		kalman_data[j++] = node_p[i]->value;
	}
	*/
	kalman_data[j] = target;
}

/* sequential identification using kalman filter algorithm */
/* note that s[][] and p[] are static variables, such that 
   their values are retained between every two function invocations */
void
kalman(zz, k, kalman_data, kalman_parameter)
int zz; /* (zz+1) is the epoch number */
int k; /* (k+1) is the times 'kalman' has been invoked in an epoch*/
double *kalman_data;
double *kalman_parameter;
{
	void s_mult_v();
	void s_mult_ma();
	double v_dot_v();
	void v_cross_v();
	void v_mult_m(); 
	void m_mult_v(); 
	void v_plus_v();
	void ma_plus_ma();

	int i, j;
	double alpha = 1000000.0;
	double denom, diff;
	static double **s;
	static double *p;
	static double *x, y;
	static double *tmp1, *tmp2;
	static double **tmp_m;

	/* initial values of s[][] and p[] if k = 0 */
	if ((zz == 0) && (k == 0)) {
		s = (double **)create_matrix(k_p_n, k_p_n, sizeof(double));
		p = (double *)create_array(k_p_n, sizeof(double));
		x = (double *)create_array(k_p_n, sizeof(double));
		tmp1 = (double *)create_array(k_p_n, sizeof(double));
		tmp2 = (double *)create_array(k_p_n, sizeof(double));
		tmp_m = (double **)create_matrix(k_p_n, k_p_n, sizeof(double));
	}
	if (k == 0) {
		for (i = 0; i < k_p_n; i++)
			p[i] = 0;

		for (i = 0; i < k_p_n; i++)
			for (j = 0; j < k_p_n; j++)
				if (i == j)
					s[i][j] = alpha; 
				else
					s[i][j] = 0;
	}

	for (i = 0; i < k_p_n; i++)
		x[i] = kalman_data[i];
	y = kalman_data[k_p_n];

	v_mult_m(x, s, tmp1);
	denom = 1 + v_dot_v(tmp1, x);
	m_mult_v(s, x, tmp1);
	v_mult_m(x, s, tmp2);
	v_cross_v(tmp1, tmp2, tmp_m);
	s_mult_ma(-1/denom, tmp_m);
	ma_plus_ma(s, tmp_m, s);

	diff = y - v_dot_v(x, p);
	m_mult_v(s, x, tmp1);
	s_mult_v(diff, tmp1);
	v_plus_v(p, tmp1, p);
	
	for (i = 0; i < k_p_n; i++)
		kalman_parameter[i] = p[i];
}

/* matrix plus matrix */
void
ma_plus_ma(m1, m2, out)
double **m1, **m2, **out;
{
	int i, j;
	for (i = 0; i < k_p_n; i++)
		for (j = 0; j < k_p_n; j++)
			out[i][j] = m1[i][j]+m2[i][j];
}

/* vector plus vector */
void
v_plus_v(v1, v2, out)
double v1[], v2[], out[];
{
	int i;
	for (i = 0; i < k_p_n; i++)
		out[i] = v1[i]+v2[i];
}

/* matrix multiplies vector */
void
m_mult_v(m, v, out)
double **m, v[], out[];
{
	int i, j;
	for (i = 0; i < k_p_n; i++) {
		out[i] = 0;
		for (j = 0; j < k_p_n; j++)
			out[i] += m[i][j]*v[j];
	}
}

/* vector multiplies matrix */
void
v_mult_m(v, m, out)
double v[], **m, out[];
{
	int i, j;
	for (i = 0; i < k_p_n; i++) {
		out[i] = 0;
		for (j = 0; j < k_p_n; j++)
			out[i] += v[j]*m[j][i];
	}
}

/* vector cross-products(?) vector */
void
v_cross_v(v1, v2, out)
double v1[], v2[], **out;
{
	int i, j;
	for (i = 0; i < k_p_n; i++)
		for (j = 0; j < k_p_n; j++)
			out[i][j] = v1[i]*v2[j];
}

/* vector dot-products vector */
double
v_dot_v(v1, v2)
double v1[], v2[];
{
	int i;
	double out = 0;
	for (i = 0; i < k_p_n; i++)
		out += v1[i]*v2[i];
	return(out);
}

/* scalar multiplies matrix */
void
s_mult_ma(c, m)
double c;
double **m;
{
	int i, j;
	for (i = 0; i < k_p_n; i++)
		for (j = 0; j < k_p_n; j++)
			m[i][j] = c*m[i][j];
}

/* scalar multiplies vector */
void
s_mult_v(c, v)
double c;
double v[];
{
	int i;
	for (i = 0; i < k_p_n; i++)
		v[i] = c*v[i];
}

/* put the parameters identified by kalman filter algorithm into GNN */
void
put_kalman_parameter(kalman_parameter)
double *kalman_parameter;
{
	/* 'first' is the index of the first node in layer 4 */
	int first = In_n + In_n*Mf_n + 2*Rule_n; 
	PARAMETER_LIST_T *p;
	int i, j = 0;

	for (i = first; i < first + Rule_n; i++) 
		for (p = node_p[i]->parameter; p != NULL; p = p->next)
			p->content = kalman_parameter[j++];
}
