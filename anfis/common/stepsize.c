/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

/* update step size */ 
void
update_step_size(error_array, i, step_size_p, decrease_rate, increase_rate)
double *error_array;
int i;
double *step_size_p;
double decrease_rate, increase_rate;
{
	static int last_decrease_ss, last_increase_ss;
	int check_increase_ss();
	int check_decrease_ss();

	if (i == 0)
		last_decrease_ss = last_increase_ss = 0;

	if (check_decrease_ss(error_array, last_decrease_ss, i)) {
		*step_size_p *= decrease_rate;
		printf("Ss decrease to %f after epoch %d.\n", *step_size_p, i+1);
		last_decrease_ss = i;
		return;
	}

	if (check_increase_ss(error_array, last_increase_ss, i)) {
		*step_size_p *= increase_rate;
		printf("Ss increase to %f after epoch %d.\n", *step_size_p, i+1);
		last_increase_ss = i;
		return;
	}
}

/* return 1 if the step size needs to be increased, 0 otherwise */
int 
check_increase_ss(error_array, last_change, current)
double *error_array;
int last_change, current;
{
	if (current - last_change < 4)
		return(0);
	if ((error_array[current]     < error_array[current - 1]) &&
	    (error_array[current - 1] < error_array[current - 2]) &&
	    (error_array[current - 2] < error_array[current - 3]) &&
	    (error_array[current - 3] < error_array[current - 4])) 
		return(1);
	return(0);
}

/* return 1 if the step size needs to be decreased, 0 otherwise */
int 
check_decrease_ss(error_array, last_change, current)
double *error_array;
int last_change, current;
{
	if (current - last_change < 4)
		return(0);
	if ((error_array[current]     < error_array[current - 1]) &&
	    (error_array[current - 1] > error_array[current - 2]) &&
	    (error_array[current - 2] < error_array[current - 3]) &&
	    (error_array[current - 3] > error_array[current - 4])) 
		return(1);
	return(0);
}
