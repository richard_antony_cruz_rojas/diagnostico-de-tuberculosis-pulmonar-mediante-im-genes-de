/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

double
epoch_error(data_matrix, data_n)
double *data_matrix;
int data_n;
{
	int j;
	double total_squared_error = 0;
	double target;
	void put_input_data();
	void calculate_output();

	if (data_n == 0) {
		printf("No data in given data matrix!\n");
		return(-1);
	}

	for (j = 0; j < data_n; j++) {
		put_input_data(j, data_matrix);
		target = data_matrix[j][In_n];
		calculate_output(In_n, Node_n - 1); 
		total_squared_error += 
			(target - node_p[Node_n - 1]->value)*
			(target - node_p[Node_n - 1]->value);
	}

	return(sqrt(total_squared_error/data_n));
}
