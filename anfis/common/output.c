/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

void
write_intermediate_parameter(i)
int i;
{
	void write_parameter();
	char destination[100];
	switch(i) {
		case 0:
			write_parameter("para.1");
			break;
		case 20:
			write_parameter("para.20");
			break;
		case 99:
			write_parameter("para.100");
			break;
		case 249:
			write_parameter("para.250");
			break;
		default:
			return;
	}
}

/* write parameters to file 'file_name' */
void
write_parameter(file_name)
char *file_name;
{
	FILE *fp;
	PARAMETER_LIST_T *p;
	int i;

	fp = open_file(file_name, "w"); 

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) 
			fprintf(fp, "%4.10lf ", p->content);
		fprintf(fp, "\n");
	}
	fclose(fp);
}

/* record the current parameters in an array */
void
record_parameter(parameter_array)
double *parameter_array;
{
	int i, j = 0;
	PARAMETER_LIST_T *p;

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) 
			parameter_array[j++] = p->content;
	}
}

/* restore the parameter in parameter_array back to ANFIS */
void
restore_parameter(parameter_array)
double *parameter_array;
{
	int i, j = 0;
	PARAMETER_LIST_T *p;

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) 
			p->content = parameter_array[j++];
	}
}
