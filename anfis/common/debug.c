/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

/* print fan-in and fan-out node, along other information */
void
debug_anfis()
{
	void print_fan_in(), print_fan_out(), print_parameter();
	int index;

	for ( ; ; ) {
		printf("Please enter node index (-1 to quit): ");
		scanf("%d", &index);
		if (index == -1)
			return;
		if (index < 0 || index > Node_n - 1) {
			printf("Invalid node index!\n");
			continue;
		}
		printf("\n");
		printf("current node: %d [value = %f, de_do = %f, layer = %d, f_index = %d]\n", 
		index, node_p[index]->value,
		node_p[index]->de_do, 
		node_p[index]->layer,
		node_p[index]->function_index);
		print_fan_in(index);
		print_fan_out(index);
		print_parameter(index);
		printf("\n");
	}
}

void
print_fan_in(index)
int index;
{
	NODE_LIST_T *p;
	if (node_p[index]->fan_in == NULL) {
		printf("No fan-in nodes!\n");
		return;
	}

	printf("fan-in nodes: ");
	for (p = node_p[index]->fan_in; p != NULL; p = p->next)
		printf("%d ", p->content->index);
	printf("\n");
}


void
print_fan_out(index)
int index;
{
	NODE_LIST_T *p;

	if (node_p[index]->fan_out == NULL) {
		printf("No fan-out nodes!\n");
		return;
	}

	printf("fan-out nodes: ");
	for (p = node_p[index]->fan_out; p != NULL; p = p->next)
		printf("%d ", p->content->index);
	printf("\n");
}

void
print_parameter(index)
int index;
{
	PARAMETER_LIST_T *p;
	if (node_p[index]->parameter == NULL) {
		printf("No parameters for this node!\n");
		return;
	}

	printf("parameter values: (");
	for (p = node_p[index]->parameter; p != NULL; p = p->next)
		printf("%f ",p->content);
	printf(")\n");
}

/* print all parameters as well as de_dp */
void
print_all_parameter()
{
	PARAMETER_LIST_T *p;
	int i;

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) {
			printf("%4.5lf ", p->content);
			printf("(%4.5lf) ", p->de_dp);
		}
		printf("\n");
	}
}
