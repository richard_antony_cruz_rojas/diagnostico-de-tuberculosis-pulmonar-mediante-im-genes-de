/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

void
input_phase(argc, argv, in_n_p, mf_n_p, 
training_data_n_p, checking_data_n_p, epoch_n_p, 
step_size_p, decrease_rate_p, increase_rate_p, debug_p)
int argc;
char **argv;
int *in_n_p, *mf_n_p;
int *training_data_n_p, *checking_data_n_p, *epoch_n_p, *debug_p;
double *step_size_p, *decrease_rate_p, *increase_rate_p;
{
        if (argc != 10) {
	     printf("Usage: %s in_n mf_n training_date_n checking_data_n epoch_n step_size decrease_rate increase_rate debug training_error_file checking_error_file\n", argv[0]);
	     exit(1);
	}

	*in_n_p = atoi(argv[1]);
	*mf_n_p = atoi(argv[2]);
        *training_data_n_p = atoi(argv[3]);
	*checking_data_n_p = atoi(argv[4]);
        *epoch_n_p = atoi(argv[5]);
        *step_size_p = atof(argv[6]);
        *decrease_rate_p = atof(argv[7]);
        *increase_rate_p = atof(argv[8]);
        *debug_p = atoi(argv[9]);
}


/* get parameters from 'parameter_file' */
void
get_parameter(parameter_file)
char *parameter_file;
{
	int i, j;
	int parameter_n;
	double tmp;
	FILE *fp;
	PARAMETER_LIST_T *p;

	fp = open_file(parameter_file, "r");

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;

		parameter_n = node_p[i]->parameter_n;
		p = node_p[i]->parameter;
		for (j = 0; j < parameter_n; j++) 
			if (fscanf(fp, "%lf", &tmp) != EOF) {
				p->content = tmp;
				p = p->next;
			}
			else
				exit1("Not enough data in 'init_parameter'!");
	}
	fclose(fp);
}


/* transfer (training or checking) data from data_file
   to matrix 'data_matrix' */
void
get_data(data_file, data_n, data_matrix)
char *data_file;
int data_n;
double **data_matrix; 
{
	int i, j;
	double tmp;
	FILE *fp;

	if (data_n == 0)
		return;

	fp = open_file(data_file, "r");

	for (i = 0; i < data_n; i++)
		for (j = 0; j < In_n + 1; j++)
			if (fscanf(fp, "%lf", &tmp) != EOF)
				data_matrix[i][j] = tmp;
			else 
				exit1("Not enough data!");
	fclose(fp);
}

/* put input part of (j+1)-th data (training or checking) to input nodes. */
void
put_input_data(j, data_matrix)
int j;
double **data_matrix;
{
	int k;
	for (k = 0; k < In_n; k++)
		node_p[k]->value = data_matrix[j][k];
}
