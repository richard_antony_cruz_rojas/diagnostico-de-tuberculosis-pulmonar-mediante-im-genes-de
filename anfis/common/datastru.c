/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

static int **config;

void
get_config()
{
	int i, j;
	int tmp;
	FILE *fp;

	fp = open_file("config", "r");
	config = (int **)create_matrix(Node_n, Node_n, sizeof(int));

	for (i = 0; i < Node_n; i++)
		for (j = 0; j < Node_n; j++)
			if (fscanf(fp, "%d", &tmp) != EOF) 
				config[i][j] = tmp;
			else
				exit1("Not enough data in 'config'.");
} 

void
build_anfis()
{
	void build_layer();

	NODE_LIST_T *build_node_list();
	int i;

	build_layer(0, In_n, 0, 0, 0); 
	build_layer(1, In_n*Mf_n, In_n, 3, 1); 
	build_layer(2, Rule_n, In_n+In_n*Mf_n, 0, 2); 
	build_layer(3, Rule_n, In_n+In_n*Mf_n+Rule_n, 0, 3); 
	build_layer(4, Rule_n, In_n+In_n*Mf_n+2*Rule_n, In_n + 1, 4); 
	build_layer(5, 1, In_n+In_n*Mf_n+3*Rule_n, 0, 5); 

	for (i = 0; i < Node_n; i++) {
		node_p[i]->fan_in  = build_node_list(0, i);
		node_p[i]->fan_out = build_node_list(1, i);
	}
}

/* Build a node list of layer-th layer, with n node, 
   starting at index index, and each
   node has parameter_n parameters and node function 
   function[function_index]. */
void
build_layer(layer, n, index, parameter_n, function_index)
int layer, n, index, parameter_n, function_index;
{
	PARAMETER_LIST_T *build_parameter_list();
	int i;
	NODE_T *q;

	if (n == 0)
		printf("Possible error in build_layer!\n");

	for (i = 0; i < n; i++) {
		q = (NODE_T *) malloc(sizeof (NODE_T));
		node_p[i + index] = q;
		q->index = i + index;
		q->layer = layer;
		q->local_index = i;
		q->parameter_n = parameter_n;
		q->function_index = function_index;
		q->parameter = build_parameter_list(parameter_n);
	}
}

/* Build a parameter list with length n. */
PARAMETER_LIST_T *
build_parameter_list(n)
int n;
{
	PARAMETER_LIST_T *head, *p, *q;
	int i;

	if (n < 0)
		exit1("Error in build_parameter_list()!");

	if (n == 0)
		return(NULL);

	head = (PARAMETER_LIST_T *) malloc(sizeof (PARAMETER_LIST_T));
	p = head;
	for (i = 1; i < n; i++){
		q = (PARAMETER_LIST_T *) malloc(sizeof (PARAMETER_LIST_T));
		p->next = q;
		p = q;
	}
	p->next = NULL;
	return(head);
}

/* type == 0 --> build node list along column n of matrix config.
   type == 1 --> build node list along row n of matrix config.     */
NODE_LIST_T *
build_node_list(type, n)
int type, n;
{
	NODE_LIST_T *p, *q, *dummy;
	int i;

	p = (NODE_LIST_T *) malloc(sizeof (NODE_LIST_T));
	dummy = p;
	dummy->next = NULL;
	if (type == 0) 
		for (i = 0; i < Node_n; i++)
			if (config[i][n]){
				q = (NODE_LIST_T *) malloc(sizeof (NODE_LIST_T));
				q->content = node_p[i];
				p->next = q;
				p = q;
			}
	if (type == 1) 
		for (i = 0; i < Node_n; i++)
			if (config[n][i]){
				q = (NODE_LIST_T *) malloc(sizeof (NODE_LIST_T));
				q->content = node_p[i];
				p->next = q;
				p = q;
			}
	p->next = NULL;
	q = dummy;
	dummy = dummy->next;
	free(q);
	return(dummy);
}

/* set parameter mode: fixed = 1 --> fixed parameter;
		       fixed = 0 --> modifiable parameter */
int
set_parameter_mode()
{
	int i, modifiable_p_count = 0;
	PARAMETER_LIST_T *p;

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) {
			p->fixed = 0;
			modifiable_p_count++;
		}
	}
	printf("Modifiable parameters: %d\n", modifiable_p_count);
	return(modifiable_p_count);
}
