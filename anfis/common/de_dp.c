/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

/* calculate de/dp of each parameters */
void
update_de_dp()
{
	int i, j;
	PARAMETER_LIST_T *p;
	double do_dp;
	double derivative_o_p();

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		j = 0;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) {
			do_dp = derivative_o_p(i, j);
			p->de_dp += (node_p[i]->de_do)*do_dp;
			j++;
		}
	}
}

/* clear de/dp of each parameters */
void
clear_de_dp()
{
	int i;
	PARAMETER_LIST_T *p;

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) 
			p->de_dp = 0;
	}
}

/* calculate the  derivative of node i w.r.t it's j-th parameter */
double
derivative_o_p(i, j)
int i, j;
{
	double dmf_dp();
	double dconsequent_dp();
	int layer = node_p[i]->layer;

	switch(layer) {
		case 1:
			return(dmf_dp(i, j));
		case 4:
			return(dconsequent_dp(i, j));
		default:
			exit1("Error in derivative_o_p!");
	}
}

double
dmf_dp(i, j)
int i, j;
{
	NODE_LIST_T *arg_p = node_p[i]->fan_in ;
	PARAMETER_LIST_T *para_p = node_p[i]->parameter;
	double c, a, b, x;
	double tmp1, tmp2;
	double denom;

	x = arg_p->content->value;
	a = para_p->content;
	b = para_p->next->content;
	c = para_p->next->next->content;
	if (a == 0)
		exit1("Error in dmf_dp!");
	tmp1 = (x - c)/a;
	tmp2 = tmp1 == 0 ? 0 : pow(pow(tmp1, 2.0), b);
	denom = (1 + tmp2)*(1 + tmp2);
	switch(j) {
		case 0: /* partial mf to partial a */
			return(2*b*tmp2/(a*denom));
		case 1: /* partial mf to partial b */
			if (tmp1 == 0)
				return(0.0);
			else
				return(-log(tmp1*tmp1)*tmp2/denom);
		case 2: /* partial mf to partial c */
			if (x == c)
				return(0.0);
			else
				return(2*b*tmp2/((x - c)*(denom)));
		default:
			exit1("Error in dmf_dp!");
	}
}

double
dconsequent_dp(i, j)
int i, j;
{
	NODE_LIST_T *arg_p = node_p[i]->fan_in ;
	PARAMETER_LIST_T *para_p = node_p[i]->parameter;
	NODE_LIST_T *a_p;
	int k;
	double wn, x;

	for (a_p = arg_p; a_p->next != NULL; a_p = a_p->next);
	wn = a_p->content->value;
	if (j == In_n)
		return(wn);
	for (a_p = arg_p, k = 0; k < j; a_p = a_p->next, k++);
	x = a_p->content->value;
	return(x*wn);
	/*
	double a, b, c, wn, x1, x2;
	x1 = arg_p->content->value;
	x2 = arg_p->next->content->value;
	wn = arg_p->next->next->content->value;
	a = para_p->content;
	b = para_p->next->content;
	c = para_p->next->next->content;
	switch(j) {
		case 0:
			return(x1*wn);
			break;
		case 1:
			return(x2*wn);
			break;
		case 2:
			return(wn);
			break;
		default:
			exit1("Error in dconsequent_dp!");
	}
	*/
}
