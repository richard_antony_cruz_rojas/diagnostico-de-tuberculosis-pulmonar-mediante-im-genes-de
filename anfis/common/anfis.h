/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "../c_lib/standard.h"

/* data structure */

typedef struct node_s {
	int index;		/* global node index within network */
	int layer;		/* which layer */
	int local_index;	/* local node index within layer */
	int parameter_n;	/* parameter no. */
	double value;		/* node value */
	double tmp;		/* for holding temporatory result */
	double de_do;		/* derivative of E to O */
	int function_index;	/* node function index  */
	struct node_list_s *fan_in;	/* list of fan_in nodes */
	struct node_list_s *fan_out;	/* list of fan_out nodes */
	struct parameter_list_s *parameter;/* list of parameters */
} NODE_T;

typedef struct integer_list_s {
	int index;
	struct integer_list_s *next;
} INTEGER_LIST_T; 

typedef struct node_list_s {
	NODE_T *content;
	struct node_list_s *next;
} NODE_LIST_T;

typedef struct parameter_list_s {
	int fixed;
	double content;
	double de_dp;
	struct parameter_list_s *next;
} PARAMETER_LIST_T;

/* global variables */
extern In_n;  /* number of input variables */
extern Mf_n;   /* number of membership functions along each input */
extern Node_n; /* number of total nodes */
extern Rule_n;    /* number of nodes in the 4-th layer */
extern NODE_T **node_p;
