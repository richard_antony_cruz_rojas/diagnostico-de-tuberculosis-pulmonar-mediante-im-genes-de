/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "anfis.h"

/* update parameters in k-th layer*/
/* if k == -1, update all parameters */
void
update_parameter(k, step_size)
int k;
double step_size;
{
	int i;
	double length;
	PARAMETER_LIST_T *p;
	double gradient_vector_length();

	length = gradient_vector_length(k);
	if (length == 0) {
		printf("gradient vector length == 0!\n");
		return;
	}
	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		if ((k != -1) && (node_p[i]->layer != k))
			continue;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) 
			p->content -= step_size*(p->de_dp)/length;
	}
}

/* calculate length of gradient vector of parameters in k-th layer */
/* if k == -1, calculate length of gradient vector of all parameters */
double
gradient_vector_length(k)
int k;
{
	int i;
	double total = 0;
	PARAMETER_LIST_T *p;

	for (i = 0; i < Node_n; i++) {
		if (node_p[i]->parameter == NULL)
			continue;
		if ((k != -1) && (node_p[i]->layer != k))
			continue;
		for (p = node_p[i]->parameter; p != NULL; p = p->next) 
			total += pow(p->de_dp, 2.0);
	}
	return(sqrt(total));
}
