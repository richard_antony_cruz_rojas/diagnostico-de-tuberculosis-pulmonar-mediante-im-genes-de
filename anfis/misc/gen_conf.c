#include <stdio.h>
#include <math.h>

/*
#define VAR_N 3  
#define MF_N 2  
#define NODE_N (VAR_N + VAR_N*MF_N + 3*MF_N*MF_N + 1)  
*/
int VAR_N, MF_N, NODE_N, RULE_N;

main(argc, argv)
int argc;
char **argv;
{
	int i, j;
	int connected();

	if (argc != 3) {
		printf("Usage: %s input_no  mf_no\n", argv[0]);
		exit(1);
	}

	VAR_N = atoi(argv[1]);
	MF_N = atoi(argv[2]);
	RULE_N = pow((double)MF_N, (double)VAR_N);
	NODE_N = VAR_N + VAR_N*MF_N + 3*RULE_N + 1;

	for (i = 0; i < NODE_N; i++){
		for (j = 0; j < NODE_N; j++)
			printf("%d ", connected(i, j));
		printf("\n");
	}
}

int
connected(i, j)
int i, j;
{
	int which_layer();
	void digit_rep();
	int layer1, layer2;
	int group;
	int position;
	int *rep;

	if (i >= j)
		return(0);
	layer1 = which_layer(i);
	layer2 = which_layer(j);
	if ((layer2 - layer1 != 1) && layer1 != 0)
		return(0);
	switch(layer1) {
		case 0: 
			if (between(VAR_N + i*MF_N, j, 
                                    VAR_N + (i+1)*MF_N - 1)) 
				return(1);
			if (between(VAR_N + VAR_N*MF_N + 2*RULE_N, j, 
				    VAR_N + VAR_N*MF_N + 3*RULE_N- 1))
				return(1);
			break;
		case 1:
			/*
			group = (int)floor((double)((i - VAR_N)/MF_N));
			*/
			rep = (int *)calloc(VAR_N, sizeof(int));
			group = (i - VAR_N)/MF_N;
			position = (i - VAR_N) % MF_N;
			digit_rep(rep, j - VAR_N - VAR_N*MF_N);
			if (rep[group] == position)
				return(1);
			break;
		case 2:
			if (between(VAR_N + VAR_N*MF_N + RULE_N, j, 
                    		VAR_N + VAR_N*MF_N + 2*RULE_N- 1))
				return(1);
			break;
		case 3:
			if (j - i == RULE_N)
				return(1);
			break;
		case 4:
			if (j == NODE_N - 1)
				return(1);
			break;
		case 5:
			return(0);
			break;
		default:
			printf("Error in layer!\n");
	}
	return(0);
}

void
digit_rep(rep, j)
int j;
int *rep;
{
	int i;
	for (i = 0; i < VAR_N; i++) {
		rep[VAR_N - i - 1] = j % MF_N;
		/* j = (int)floor((double)(j/MF_N)); */
		j = j/MF_N;
	}
}
		
int 
which_layer(i)
int i;
{
	int between();
	if (between(0, i, VAR_N - 1))
		return(0);
	if (between(VAR_N, i, VAR_N + VAR_N*MF_N - 1))
		return(1);
	if (between(VAR_N + VAR_N*MF_N, i, 
                    VAR_N + VAR_N*MF_N + RULE_N- 1))
		return(2);
	if (between(VAR_N + VAR_N*MF_N + RULE_N, i, 
                    VAR_N + VAR_N*MF_N + 2*RULE_N- 1))
		return(3);
	if (between(VAR_N + VAR_N*MF_N + 2*RULE_N, i, 
                    VAR_N + VAR_N*MF_N + 3*RULE_N- 1))
		return(4);
	if (i = VAR_N + VAR_N*MF_N + 3*RULE_N)
		return(5);
	printf("Error in which_layer!\n");
	exit(1);
}

int
between(l, x, u)
int l, x, u;
{
	if ((l <= x) && (x <= u))
		return(1);
	else 
		return(0);
}
