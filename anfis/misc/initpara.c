/* generate initial parameters according to training data */

#include "/usr2/grad/jang/c_lib/standard.h"

main(argc, argv)
int argc;
char **argv;
{
	char *data_file;
	int data_n, col_n, mf_n;
	int i, j;
	FILE *fp;
	double **min_max; /* 1st row = min, 2nd row = max */
	double max = pow(2.0, 31.0) - 1;
	double min = -max;
	double rule_n; 
	double a, b, c;
	double tmp;
	void compare();

	if (argc != 5) {
		printf("Usage: %s training_data_file data_n col_n mf_n\n", argv[0]);
		exit(1);
	}
	data_file = argv[1];
	data_n = atoi(argv[2]);
	col_n = atoi(argv[3]);
	mf_n = atoi(argv[4]);

	rule_n = pow((double)mf_n, (double)(col_n - 1));
	min_max = (double **)create_matrix(2, col_n, sizeof(double));
	for (i = 0; i < col_n; i++) {
		min_max[0][i] = max; 
		min_max[1][i] = min; 
	}

	fp = fopen(data_file, "r");
	for (i = 0; i < data_n; i++)
		for (j = 0; j < col_n; j++) {
				if (fscanf(fp, "%lf", &tmp) != EOF) 
					compare(tmp, j, min_max);
				else break;
		}
	/*
	printf("min and max of each column:\n\n");
	print_matrix(min_max, 2, col_n);
	printf("\n");
	*/
	fclose(fp);

	/*
	printf("Premise parameters:\n\n");
	*/
	for (i = 0; i < col_n - 1; i++) {
		a = (min_max[1][i] - min_max[0][i])/(2*mf_n - 2);
		b = 2.0;
		for (j = 0; j < mf_n; j++) {
			c = min_max[0][i] + a*2*j;
			printf("%lf %lf %lf\n", a, b, c);
		}
	}
	/*
	printf("Consequent parameters:\n");
	printf("(Rule number = %d)\n\n", (int)rule_n);
	*/
	for (i = 0; i < rule_n; i++) {
		for (j = 0; j < col_n; j++)
			printf("%d ", 0);
		printf("\n");
	}
}

void
compare(value, i, min_max)
double value;
int i;
double **min_max;
{
	if (value < min_max[0][i])
		min_max[0][i] = value;
	if (value > min_max[1][i])
		min_max[1][i] = value;
}

char **
create_matrix(row_n, col_n, element_size)
int row_n, col_n, element_size;
{
	char **matrix;
	int i;

	matrix = (char **) malloc(sizeof(char *)*row_n);
	if (matrix == NULL) {
		printf("Error in create_matrix!\n");
		exit(1);
	}
	for (i = 0; i < row_n; i++) { 
		matrix[i] = (char *) malloc(element_size*col_n);
		if (matrix[i] == NULL) {
			printf("Error in create_matrix!\n");
			exit(1);
		}
	}
	return(matrix);
}
