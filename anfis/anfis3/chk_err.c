/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "../common/anfis.h"

/* error_index[0] = RMSE --> root mean square error
   error_index[1] = NDEI --> non-dimensional error index
   error_index[2] = ARV  --> average relative variance 
   error_index[3] = APE  --> average percentage error */
void
checking_error_measure(data_matrix, anfis_output, data_n, error_index)
double **data_matrix, *anfis_output;
int data_n;
double error_index[];
{
	double RMSE, NDEI, ARV, APE;
	int i;
	static double target_total, abs_target_total, target_mean;
	static double target_VAR, target_STD, tmp;
	static int initialization;
	double total_abs_error, total_squared_error;
	double diff;
	
	/* initialization for the first call of this function */
	if (initialization != 7527474) {
		initialization = 7527474;
		target_total = 0;
		abs_target_total = 0;
		target_VAR = 0;
		target_STD = 0;
		for (i = 0; i < data_n; i++) {
			target_total += data_matrix[i][In_n];
			abs_target_total += ABS(data_matrix[i][In_n]);
		}
		target_mean = target_total/data_n;
		tmp = 0;
		for (i = 0; i < data_n; i++)
			tmp += (data_matrix[i][In_n] - target_mean)*
				(data_matrix[i][In_n] - target_mean);
		target_VAR = tmp/data_n;
		target_STD = sqrt(target_VAR);
	}

	total_abs_error = 0;
	total_squared_error = 0;
	tmp = 0;
	for (i = 0; i < data_n; i++) {
		diff = data_matrix[i][In_n] - anfis_output[i];
		total_abs_error += ABS(diff);
		total_squared_error += (diff*diff);
		tmp += (ABS(diff)/data_matrix[i][In_n]);
	}
	RMSE = sqrt(total_squared_error/data_n);
	NDEI = RMSE/target_STD;
	ARV = NDEI*NDEI;
	APE = total_abs_error/abs_target_total;
	error_index[0] = RMSE;
	error_index[1] = NDEI;
	error_index[2] = ARV;
	error_index[3] = APE;
	/*
	error_index[3] = tmp/data_n;
	*/
}

void
epoch_checking_error(data_matrix, data_n, error_index)
double **data_matrix;
int data_n;
double error_index[];
{
	int j;
	static int initialization;
	static double *anfis_output;

	void put_input_data();
	void calculate_output();
	void checking_error_measure();

	if (data_n == 0) {
		printf("No data in given data matrix!\n");
		exit(1);
	}

	/* allocate array when first called */ 
	if (initialization != 7527474) {
		initialization = 7527474;
		anfis_output = (double *)calloc(data_n, sizeof(double));
	}

	for (j = 0; j < data_n; j++) {
		put_input_data(j, data_matrix);
		calculate_output(In_n, Node_n - 1); 
		anfis_output[j] = node_p[Node_n - 1]->value;
	}
	checking_error_measure(data_matrix, anfis_output, data_n, error_index);
}
