/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "../common/anfis.h"
#include "../common/misc_def.h"

NODE_T **node_p;
int In_n, Mf_n, Rule_n, Node_n;

main(argc, argv)
int argc;
char **argv;
{
	int i, j, k, debug;
	int training_data_n, checking_data_n, epoch_n, parameter_n;
	double increase_rate, decrease_rate, step_size;
	double min_RMSE = pow(2.0, 31.0) - 1;
	double *trn_rmse_error, *chk_rmse_error;
	double *kalman_data, *kalman_parameter;
	double de_dout, target;
	double **layer_1_to_3_output;
	double **training_data_matrix, **checking_data_matrix;
	double *parameter_array, *step_size_array, *anfis_output;
	double *trn_error, *chk_error;

	input_phase(argc, argv, &In_n, &Mf_n,
		&training_data_n, &checking_data_n, &epoch_n,
		&step_size, &decrease_rate, &increase_rate, &debug);

	Rule_n = (int)pow((double)Mf_n, (double)In_n);
	Node_n = In_n + In_n*Mf_n + 3*Rule_n + 1;

	/* allocate matrices */
	node_p = (NODE_T **)create_array(Node_n, sizeof(NODE_T *));
	training_data_matrix = (double **)create_matrix
			(training_data_n, In_n + 1, sizeof(double));
	checking_data_matrix = (double **)create_matrix
			(checking_data_n, In_n + 1, sizeof(double));
	layer_1_to_3_output = (double **)create_matrix
		(training_data_n, In_n*Mf_n + 2*Rule_n, sizeof(double));
	trn_rmse_error = (double *)calloc(epoch_n, sizeof(double));
	chk_rmse_error = (double *)calloc(epoch_n, sizeof(double));
	kalman_data = (double *)
		      calloc((In_n + 1)*Rule_n + 1, sizeof(double));
	kalman_parameter = (double *)calloc
			   ((In_n + 1)*Rule_n, sizeof(double));
	step_size_array = (double *)calloc(epoch_n, sizeof(double));
	anfis_output = (double *)calloc(training_data_n, sizeof(double));
	trn_error = (double *)calloc(4, sizeof(double));
	chk_error = (double *)calloc(4, sizeof(double));

	get_config();
	build_anfis();
	parameter_n = set_parameter_mode();
	parameter_array = (double *)calloc(parameter_n, sizeof(double));
	get_parameter(INIT_PARA_FILE);
	get_data(TRAIN_DATA_FILE, training_data_n, training_data_matrix);
	get_data(CHECK_DATA_FILE, checking_data_n, checking_data_matrix);
	if (debug != 0) debug_anfis();

	printf("epochs \t trn error \t chk error\n");
	printf("------ \t --------- \t ---------\n");
	for (i = 0; i < epoch_n; i++) {
		step_size_array[i] = step_size;
		for (j = 0; j < training_data_n; j++) {
			put_input_data(j, training_data_matrix);
			/* get node outputs from layer 1 to layer 3 */
			calculate_output(In_n, In_n + In_n*Mf_n + 2*Rule_n - 1);
			/* put outputs of layer 1 to 3 
			   into layer_1_to_3_output */
			for (k = 0; k < Mf_n*In_n + 2*Rule_n; k++)
				layer_1_to_3_output[j][k] 
					= node_p[k + In_n]->value;
			target = training_data_matrix[j][In_n]; 
			get_kalman_data(kalman_data, target);
			kalman(i, j, kalman_data, kalman_parameter);
		}
		put_kalman_parameter(kalman_parameter);
		write_intermediate_parameter(i);  
		clear_de_dp();
		for (j = 0; j < training_data_n; j++) {
			put_input_data(j, training_data_matrix);
			/* get output of layer 1 to 3 
			   from layer_1_to_3_output */
			for (k = 0; k < Mf_n*In_n + 2*Rule_n; k++)
				node_p[k + In_n]->value 
					= layer_1_to_3_output[j][k]; 
			/* calculate outputs of layer 4 and 5 */
			calculate_output(In_n + In_n*Mf_n + 2*Rule_n, Node_n - 1); 
			anfis_output[j] = node_p[Node_n - 1]->value;
			target = training_data_matrix[j][In_n]; 
			de_dout = -2*(target - node_p[Node_n-1]->value);
			calculate_de_do(de_dout);
			update_de_dp();
		}
		training_error_measure(training_data_matrix, anfis_output,
			training_data_n, trn_error);
		trn_rmse_error[i] = trn_error[0];

		if (checking_data_n != 0) {
			epoch_checking_error(checking_data_matrix,
				checking_data_n, chk_error);
			chk_rmse_error[i] = chk_error[0];
			
			printf("%3d \t %lf \t %lf\n", i+1,
				trn_error[ERROR_TYPE],
				chk_error[ERROR_TYPE]);
		} else
			printf("%4d \t %lf\n", i+1,
				trn_error[ERROR_TYPE]);

		if (trn_rmse_error[i] < min_RMSE) {
			min_RMSE = trn_rmse_error[i];
			record_parameter(parameter_array);
		}
		/* update parameters in 1st layer */
		update_parameter(1, step_size);

		update_step_size(trn_rmse_error, i, &step_size, 
			decrease_rate, increase_rate);

	}
	printf("Minimal training RMSE = %lf\n", min_RMSE);
	restore_parameter(parameter_array);
	write_parameter(FINA_PARA_FILE);
	write_array(trn_rmse_error, epoch_n, TRAIN_ERR_FILE);
	if (checking_data_n != 0)
		write_array(chk_rmse_error, epoch_n, CHECK_ERR_FILE);
	write_array(step_size_array, epoch_n, STEP_SIZE_FILE);
}
