/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "standard.h"

/* the following uses calloc instead of malloc */
/* malloc version is in lib.c */
char **
create_matrix(row_n, col_n, element_size)
int row_n, col_n, element_size;
{
	char **matrix;
	int i;

	matrix = (char **)calloc(row_n, sizeof(char *));
	if (matrix == NULL) {
		printf("Error in create_matrix!\n");
		exit(1);
	}
	for (i = 0; i < row_n; i++) { 
		matrix[i] = (char *) calloc(col_n, element_size);
		if (matrix[i] == NULL) {
			printf("Error in create_matrix!\n");
			exit(1);
		}
	}
	return(matrix);
}

/* 'file' is relative to the home directory */
FILE *
open_file1(file, mode)
char *file, *mode;
{
	char *home;
	char new[100];
	FILE *fp, *fopen();

	home = (char *)getenv("HOME");
	strcpy(new, home);
	strcat(strcat(new, "/"), file);
	if ((fp = fopen(new, mode)) == NULL){
		printf("Cannot open '%s'.\n", new);
		exit(1);
	}
	return(fp);
}

/* 'file' is relative to the current directory */
FILE *
open_file2(file, mode)
char *file, *mode;
{
	char pwd[100];
	void get_pwd();
	FILE *fp, *fopen();

	get_pwd(pwd);
	strcat(strcat(pwd, "/"), file);
	if ((fp = fopen(pwd, mode)) == NULL){
		printf("Cannot open '%s'.\n", pwd);
		exit(1);
	}
	return(fp);
}

/* write array to file 'file_name', using matlab format */
/* the matlab variable name is the baseanme of 'file_name' */
void
write_array(array, size, file_name)
double *array;
int size;
char *file_name;
{
	FILE *fp;
	int i;

	fp = open_file(file_name, "w");
	fprintf(fp, "%s = [\n", basename(file_name));
	for (i = 0; i < size; i++) 
		if (i != size - 1)
			fprintf(fp, "%d %lf;\n", i+1, array[i]);
		else 
			fprintf(fp, "%d %lf];\n", i+1, array[i]);
	fclose(fp);
}

/* get current directory */
/* same as getcwd() */
char *
get_pwd()
{
	char pwd[100];
	FILE *pwdpipe;
	FILE *popen();

	pwdpipe = popen("pwd", "r");
	if (pwdpipe == NULL) {
		printf("Cannot get pwd!\n");
		exit(1);
	}
	fscanf(pwdpipe, "%s", pwd);
	pclose(pwdpipe);
	return(pwd);
}

/* create a directory */
/* same as mkdir() */
void 
create_dir(directory)
char *directory;
{
	char cmd[50];

	sprintf(cmd, "/bin/mkdir %s", directory);
	if (system(cmd) != 0) {
		printf("Error in create_dir(%s)!\n", directory); 
		exit(1);
	}
}
