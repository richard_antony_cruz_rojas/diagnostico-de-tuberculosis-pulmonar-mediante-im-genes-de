/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "standard.h"
/* you can set random seed by using srandom() */

#define MAXIMUM (pow(2.0, 31.0) - 1)

/* return a random number between 0 and 1 */
double
random1()
{
	return(random()/MAXIMUM);
}

/* return a random number between -1 and 1 */
double
random2()
{
	return((random() - MAXIMUM/2.0)/(MAXIMUM/2.0));
}
