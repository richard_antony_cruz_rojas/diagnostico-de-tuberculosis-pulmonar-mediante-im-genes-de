/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

/* vector operations */

/* matrix multiplies vector */
void
m_mult_v(m, v, row_n, col_n, out)
double **m, *v;
int row_n, col_n;
double *out;
{
	int i, j;
	for (i = 0; i < row_n; i++) {
		out[i] = 0;
		for (j = 0; j < col_n; j++)
			out[i] += m[i][j]*v[j];
	}
}

/* vector plus vector */
void
v_plus_v(v1, v2, size, out)
double *v1, *v2;
int size;
double *out;
{
	int i;
	for (i = 0; i < size; i++)
		out[i] = v1[i]+v2[i];
}

/* vector multiplies matrix */
void
v_mult_m(v, m, row_n, col_n, out)
double *v, **m;
int row_n, col_n;
double *out;
{
	int i, j;
	for (i = 0; i < col_n; i++) {
		out[i] = 0;
		for (j = 0; j < row_n; j++)
			out[i] += v[j]*m[j][i];
	}
}

/* vector cross-products(?) vector */
void
v_cross_v(v1, v2, size1, size2, out)
double *v1, *v2;
int size1, size2;
double **out;
{
	int i, j;
	for (i = 0; i < size1; i++)
		for (j = 0; j < size2; j++)
			out[i][j] = v1[i]*v2[j];
}

/* vector dot-products vector */
double
v_dot_v(v1, v2, size)
double *v1, *v2;
int size;
{
	int i;
	double out = 0;
	for (i = 0; i < size; i++)
		out += v1[i]*v2[i];
	return(out);
}

/* scalar multiplies vector */
void
s_mult_v(c, v, size)
double c;
double *v;
int size;
{
	int i;
	for (i = 0; i < size; i++)
		v[i] = c*v[i];
}
