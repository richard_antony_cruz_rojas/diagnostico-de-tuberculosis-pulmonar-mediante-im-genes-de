/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

#include "standard.h"

static double **S, **P, **a, **b, **a_t, **b_t;
static double **tmp1, **tmp2, **tmp3, **tmp4;
static double **tmp5, **tmp6, **tmp7;

/* create necessary matrices */ 
/* this function has to be invoked once and only  once
   in a program which invokes LSE_kalman */
/* an example can be found in ~/LSE/kalman/main.c */

void
initialize_kalman(in_n, out_n)
int in_n, out_n;
{
	S = (double **)create_matrix(in_n, in_n, sizeof(double));
	P = (double **)create_matrix(in_n, out_n, sizeof(double));
	a = (double **)create_matrix(in_n, 1, sizeof(double));
	b = (double **)create_matrix(out_n, 1, sizeof(double));
	a_t = (double **)create_matrix(1, in_n, sizeof(double));
	b_t = (double **)create_matrix(1, out_n, sizeof(double));

	tmp1 = (double **)create_matrix(in_n, 1, sizeof(double));
	tmp2 = (double **)create_matrix(1, 1, sizeof(double));
	tmp3 = (double **)create_matrix(1, in_n, sizeof(double));
	tmp4 = (double **)create_matrix(in_n, in_n, sizeof(double));
	tmp5 = (double **)create_matrix(1, out_n, sizeof(double));
	tmp6 = (double **)create_matrix(in_n, out_n, sizeof(double));
	tmp7 = (double **)create_matrix(in_n, out_n, sizeof(double));
}

/* reset values of S[][] and P[][] */
void
reset_kalman(in_n, out_n)
int in_n, out_n;
{
	int i, j;
	double alpha = 1e6;

	for (i = 0; i < in_n; i++)
		for (j = 0; j < out_n; j++)
			P[i][j] = 0;

	for (i = 0; i < in_n; i++)
		for (j = 0; j < in_n; j++)
			if (i == j)
				S[i][j] = alpha; 
			else
				S[i][j] = 0;
}

/* sequential identification using kalman filter algorithm */
/* kalman_data_pair is an array composed of desired input and output */
void
kalman(in_n, out_n, kalman_data_pair, kalman_parameter)
int in_n, out_n;
double *kalman_data_pair;
double **kalman_parameter;
{
	int i, j;
	double denom;

	for (i = 0; i < in_n; i++)
		a[i][0] = kalman_data_pair[i];
	for (i = 0; i < out_n; i++)
		b[i][0] = kalman_data_pair[i+in_n];

	m_transpose(a, in_n, 1, a_t);
	m_transpose(b, out_n, 1, b_t);

	/* recursive formulas for S, covariance matrix */
	m_times_m(S, a, in_n, in_n, 1, tmp1);
	m_times_m(a_t, tmp1, 1, in_n, 1, tmp2);
	denom = 1 + tmp2[0][0];
	m_times_m(a_t, S, 1, in_n, in_n, tmp3);
	m_times_m(tmp1, tmp3, in_n, 1, in_n, tmp4);
	s_times_m(1/denom, tmp4, in_n, in_n, tmp4);
	m_minus_m(S, tmp4, in_n, in_n, S);

	/* recursive formulas for P, the estimated parameter matrix */
	m_times_m(a_t, P, 1, in_n, out_n, tmp5);
	m_minus_m(b_t, tmp5, 1, out_n, tmp5);
	m_times_m(a, tmp5, in_n, 1, out_n, tmp6);
	m_times_m(S, tmp6, in_n, in_n, out_n, tmp7);
	m_plus_m(P, tmp7, in_n, out_n, P);

	for (i = 0; i < in_n; i++)
		for (j = 0; j < out_n; j++)
			kalman_parameter[i][j] = P[i][j];
}

/* same as kalman(), but with forgetting factor lambda */
void
new_kalman(in_n, out_n, kalman_data_pair, kalman_parameter, lambda)
int in_n, out_n;
double *kalman_data_pair;
double **kalman_parameter;
double lambda;
{
	int i, j;
	double denom;

	for (i = 0; i < in_n; i++)
		a[i][0] = kalman_data_pair[i];
	for (i = 0; i < out_n; i++)
		b[i][0] = kalman_data_pair[i+in_n];

	m_transpose(a, in_n, 1, a_t);
	m_transpose(b, out_n, 1, b_t);

	/* recursive formulas for S, covariance matrix */
	m_times_m(S, a, in_n, in_n, 1, tmp1);
	m_times_m(a_t, tmp1, 1, in_n, 1, tmp2);
	denom = lambda + tmp2[0][0];
	m_times_m(a_t, S, 1, in_n, in_n, tmp3);
	m_times_m(tmp1, tmp3, in_n, 1, in_n, tmp4);
	s_times_m(1/denom, tmp4, in_n, in_n, tmp4);
	m_minus_m(S, tmp4, in_n, in_n, S);
	s_times_m(1/lambda, S, in_n, in_n, S);

	/* recursive formulas for P, the estimated parameter matrix */
	m_times_m(a_t, P, 1, in_n, out_n, tmp5);
	m_minus_m(b_t, tmp5, 1, out_n, tmp5);
	m_times_m(a, tmp5, in_n, 1, out_n, tmp6);
	m_times_m(S, tmp6, in_n, in_n, out_n, tmp7);
	m_plus_m(P, tmp7, in_n, out_n, P);

	for (i = 0; i < in_n; i++)
		for (j = 0; j < out_n; j++)
			kalman_parameter[i][j] = P[i][j];
}

/* find the least square solution of AX = B, 
   using kalman filter algorithm */
/* return the RMSE (root mean squares error) */
/* A: data_n X in_n
   X: in_n X out_n
   B: data_n X out_n */
double
LSE_kalman(A, B, X, data_n, in_n, out_n, lambda)
double **A, **B, **X;
int data_n, in_n, out_n;
double lambda;
{
	double *kalman_data_pair;
	double **AX, **AX_minus_B;
	double root_square_error;
	int i;
	static int initialization;

	void get_kalman_data();
	void initialize_kalman();
	void reset_kalman();
	void kalman(), new_kalman();

	kalman_data_pair = (double *)calloc(in_n+out_n, sizeof(double));
	AX = (double **)create_matrix(data_n, out_n, sizeof(double));
	AX_minus_B = (double **)
		create_matrix(data_n, out_n, sizeof(double));

	/* initialize only once, at the first call of this function */
	if (initialization != 7527474) {
		initialize_kalman(in_n, out_n);
		initialization = 7527474;
	}
	reset_kalman(in_n, out_n);
	for (i = 0; i < data_n; i++) {
		get_kalman_data(A, B, i, kalman_data_pair, in_n, out_n);
		new_kalman(in_n, out_n, kalman_data_pair, X, lambda);
	}

	m_times_m(A, X, data_n, in_n, out_n, AX);
	m_minus_m(AX, B, data_n, out_n, AX_minus_B);
	root_square_error = m_norm(AX_minus_B, data_n, out_n);

	free_array(kalman_data_pair);
	free_matrix(AX, data_n);
	free_matrix(AX_minus_B, data_n);

	return(root_square_error/sqrt((double)(data_n)));
}

/* get the k-th data entry to be used in kalman filter algorithm */
void
get_kalman_data(A, B, k, kalman_data_pair, in_n, out_n)
double **A, **B;
double *kalman_data_pair;
int k, in_n, out_n;
{
	int i;
	for (i = 0; i < in_n ; i++) 
		kalman_data_pair[i] = A[k][i];
	for (i = 0; i < out_n ; i++) 
		kalman_data_pair[i+in_n] = B[k][i];
}
