/*
   Copyright (c) 1991 Jyh-Shing Roger Jang, Dept of EECS, U.C. Berkeley
   BISC (Berkeley Initiative on Soft Computing) group
   jang@eecs.berkeley.edu
   Permission is granted to modify and re-distribute this code in any manner
   as long as this notice is preserved.  All standard disclaimers apply.
*/

/* standard header files */
#include <stdio.h>
#include <strings.h>
/* #include <stdlib.h> */
#include <math.h>

/* handy macros */
#define ABS(x)   ( x > 0 ? (x): (-(x)) )
#define MAX(x,y) ( x > y ? (x) : (y) )
#define MIN(x,y) ( x < y ? (x) : (y) )
#define SGN(x)   ( x > 0 ? (1) : (-1) )

/* library function declaration */
/* memory allocation */
char *create_array();
char **create_matrix();
char ***create_cubic();
void free_array();
void free_matrix();
void free_cubic();

/* print to stdio and write to files */
void print_array();
void print_matrix();
void write_array();
void write_matrix();

/* file open */
FILE *open_file1();
FILE *open_file2();

/* matrix functions */
void m_plus_m();
void m_minus_m();
void m_times_m();
void s_mult_m();
void s_times_m();
void m_transpose();
double m_norm();

/* least square estimate */
void initialize_kalman();
double LSE_kalman();

/* random number generator */
double random1();
double random2();

/* others */
void exit1();
char *basename();
